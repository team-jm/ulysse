<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\BackBundle\Entity\Categorie;
use Ulysse\BackBundle\Form\CategorieType;

/**
 * Categorie controller.
 *
 * @Route("/categorie")
 */
class CategorieController extends Controller
{

    /**
     * Lists all Categorie entities.
     *
     * @Route("/", name="categorie")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UlysseBackBundle:Categorie')->findAll();
        return array('entities' => $entities,);
    }
    
    /**
     * Creates a new Categorie entity.
     *
     * @Route("/", name="categorie_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:Categorie:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Categorie();
        $form = $this->createForm(new CategorieType(), $entity, array('action' => $this->generateUrl('back_category_create'),
                                                                      'method' => 'POST',));
        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('back_categories', array('id' => $entity->getId())));
        }

        return $this->render('UlysseBackBundle:Categorie:create.html.twig', array('entity' => $entity, 'form'   => $form->createView(),));
    }

    /**
     * Finds and displays a Categorie entity.
     *
     * @Route("/{id}", name="categorie_show")
     * @Method("GET")
     * @Template()
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UlysseBackBundle:Categorie')->find($id);
        
        $sous_categories = $em->getRepository('UlysseBackBundle:Sous_categorie')->findby(array('categorie' => $entity));
        
        $array_article = array();
        
        if($sous_categories){
            foreach($sous_categories as $sous_categorie){
                $articles = $em->getRepository('UlysseBackBundle:Article')->findby(array('sous_categorie' => $sous_categorie));
                if($articles){
                   $array_article[]  = $articles;
                }
            }
        }
        //var_dump($array_article);exit();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        return $this->render('UlysseBackBundle:Categorie:detail.html.twig',array('entity' => $entity,'sous_categories'   => $sous_categories, 'articles' => $array_article));
    }

    /**
     * Edits an existing Categorie entity.
     *
     * @Route("/{id}", name="categorie_update")
     * @Method("PUT")
     * @Template("UlysseBackBundle:Categorie:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }
        
        $editForm = $this->createForm(new CategorieType(), $entity, array(
            'action' => $this->generateUrl('back_category_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $editForm->add('submit', 'submit', array('label' => 'Update'));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_category_detail', array('id' => $id)));
        }

       return $this->render('UlysseBackBundle:Categorie:update.html.twig',array('entity' => $entity,'edit_form'   => $editForm->createView(),));
    }
   
    /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('UlysseBackBundle:Categorie')->find($id);
       
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('back_categories'));
    }
    
}
