<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\BackBundle\Entity\Sous_categorie;
use Ulysse\BackBundle\Form\Sous_categorieType;

/**
 * Sous-Categorie controller.
 *
 * @Route("/sub_categories")
 */
class SousCategorieController extends Controller
{
    
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UlysseBackBundle:Sous_categorie')->findAll();

        return $this->render('UlysseBackBundle:SousCategorie:index.html.twig', array('entities' => $entities));
    }
    
    /**
     * Creates a new Sous-Categorie entity.
     *
     * @Route("/", name="categorie_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:SousCategorie:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Sous_categorie();
        $form = $this->createForm(new Sous_categorieType(), $entity, array( 'action' => $this->generateUrl('back_sub_category_create'),
                                                                            'method' => 'POST',));
        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('back_sub_category_detail', array('id' => $entity->getId())));
        }
        
       return $this->render('UlysseBackBundle:SousCategorie:create.html.twig', array('entity' => $entity,'form'   => $form->createView(),));
    }

    /**
     * Finds and displays a Categorie entity.
     *
     * @Route("/{id}", name="back_category_detail")
     * @Method("GET")
     * @Template("UlysseBackBundle:SousCategorie:detail.html.twig")
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UlysseBackBundle:Sous_categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sous_categorie entity.');
        }
        
        return array('entity'  => $entity,);
    }
    
    /**
     * Edits an existing Categorie entity.
     *
     * @Route("/{id}", name="back_category_update")
     * @Method("PUT")
     * @Template("UlysseBackBundle:Categorie:update.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Sous_categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sous_categorie entity.');
        }
        
        $editForm = $this->createForm(new Sous_categorieType(), $entity, array(
            'action' => $this->generateUrl('back_sub_category_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $editForm->add('submit', 'submit', array('label' => 'Update'));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_sub_category_detail', array('id' => $id)));
        }

       return $this->render('UlysseBackBundle:SousCategorie:update.html.twig',array('entity' => $entity,'form'   => $editForm->createView(),));
    }
    
    /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('UlysseBackBundle:Sous_categorie')->find($id);
       
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('back_sub_categories'));
    }
}