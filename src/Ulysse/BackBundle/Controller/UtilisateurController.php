<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\UserBundle\Entity\Utilisateur;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;

class UtilisateurController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseUserBundle:Utilisateur')->findAll();

        return $this->render('UlysseBackBundle:Utilisateur:index.html.twig', array(
            'users' => $entities
        ));
    }
    public function createAction(Request $request)
    {
        $entity = new Utilisateur();
        $entity->setEnabled(true);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('back_user_detail', array('id' => $entity->getId())));
        }
      //  var_dump($form);die;
        return $this->render('UlysseBackBundle:Utilisateur:new.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    private function createCreateForm(Utilisateur $entity)
    {
        $form = $this->createForm($this->get('ulysse.registration.form.type'), $entity, array(
            'action' => $this->generateUrl('back_user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }
    
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseUserBundle:Utilisateur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Utilisateur entity.');
        }
        
        $ville = $em->getRepository('UlysseUserBundle:Ville')->findBy(array('nom' => $entity->getVille()));
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UlysseBackBundle:Utilisateur:detail.html.twig', array(
            'user'      => $entity,
            'ville'     => $ville,
            'delete_form' => $deleteForm->createView(),
        ));  
    }
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseUserBundle:Utilisateur')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Utilisateur entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('back_user_update', array('id' => $id)));
        }

        return $this->render('UlysseBackBundle:Utilisateur:update.html.twig', array(
            'user'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    private function createEditForm(Utilisateur $entity)
    {
        $form = $this->createForm($this->get('ulysse.profile.form.type'), $entity, array(
            'action' => $this->generateUrl('back_user_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UlysseUserBundle:Utilisateur')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Utilisateur entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('back_users'));  
    }
    /**
     * Creates a form to delete a Utilisateur entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('back_user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}