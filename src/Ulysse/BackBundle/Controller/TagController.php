<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\BackBundle\Entity\Tag;
use Ulysse\BackBundle\Form\TagType;

/**
 * Tag controller.
 *
 * @Route("/tag")
 */
class TagController extends Controller
{

    /**
     * Lists all Tag entities.
     *
     * @Route("/", name="tag")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UlysseBackBundle:Tag')->findAll();

        return array('entities' => $entities,);
    }
    /**
     * Creates a new Tag entity.
     *
     * @Route("/", name="back_tag_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:Tag:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Tag();
        $form = $this->createForm(new TagType(), $entity, array('action' => $this->generateUrl('back_tag_create'),
                                                                'method' => 'POST',));

        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('back_tag_detail', array('id' => $entity->getId())));
        }

        return $this->render('UlysseBackBundle:Tag:create.html.twig', array('entity' => $entity,'form'   => $form->createView(),));
    }
    
    /**
     * Finds and displays a Tag entity.
     *
     * @Route("/{id}", name="back_tag_detail")
     * @Method("GET")
     * @Template()
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Tag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tag entity.');
        }

        return array('entity'      => $entity,);
    }

    /**
     * Edits an existing Tag entity.
     *
     * @Route("/{id}", name="tag_update")
     * @Method("PUT")
     * @Template("UlysseBackBundle:Tag:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Tag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tag entity.');
        }

        $editForm = $this->createForm(new TagType(), $entity, array('action' => $this->generateUrl('back_tag_update', array('id' => $entity->getId())),
                                                                     'method' => 'PUT', ));

        $editForm->add('submit', 'submit', array('label' => 'Update'));

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_tag_detail', array('id' => $id)));
        }

         return $this->render('UlysseBackBundle:Tag:update.html.twig',  array('entity'      => $entity,'form'   => $editForm->createView(),));
    }
    
     /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('UlysseBackBundle:Tag')->find($id);
       
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('back_tags'));
    }
}
