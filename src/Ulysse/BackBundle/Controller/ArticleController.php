<?php

namespace Ulysse\BackBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ulysse\BackBundle\Entity\Article;
use Ulysse\BackBundle\Entity\Image;
use Ulysse\BackBundle\Entity\Tag;
use Ulysse\BackBundle\Form\ArticleType;
use Ulysse\BackBundle\Form\ImageType;
use Ulysse\BackBundle\Form\TagType;

/**
 * Article controller.
 *
 * @Route("/article")
 */
class ArticleController extends Controller
{

    /**
     * Lists all Article entities.
     *
     * @Route("/", name="article")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('UlysseBackBundle:Article')->findAll();

        return array(
            'entities' => $entities,
        );
    }
   
 /**
     * Creates a new Article entity.
     *
     * @Route("/", name="article_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:Article:new.html.twig")
     */
    public function createAction(Request $request)
    {
        
        $article = new Article();
        
        //-- ajout de tag
       $tag1 = new Tag();
       $article->getTag()->add($tag1);
       
       $tag2 = new Tag();
       $article->getTag()->add($tag2);
        
       $image = new Image();
       $image->setArticle($article); 

       //-- on recupérè l'admin connecté
       $admin = $this->get('security.context')->getToken()->getUser();
       $role = $this->get('security.context')->isGranted('ROLE_ADMIN');
            
       $form = $this->createForm(new ImageType($role), $image, array('action' => $this->generateUrl('back_article_create'),
                                                                       'method' => 'POST',));
        
       $form->add('submit', 'submit', array('label' => 'Create'));
       $form->handleRequest($request);
        
       if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $user = $em->getRepository('UlysseUserBundle:Utilisateur')->find($admin->getId());
            
            //-- on associe l'admin à l'article
            $image->getArticle()->setCreateur($user);
            
            $image->getArticle()->setValided($user);
            
            //- on y intègre la date de création actuelle
            $image->getArticle()->setDateCreation(new \DateTime());    

            $em->persist($image);
            $em->flush();

             $this->addFlash( 'information',"L'article « ".$image->getArticle()->getNom()."» a bien été crée");
             
            //$this->get('session')->getFlashBag()->add('information',"L'article ");
            return $this->redirect($this->generateUrl('back_articles'));
        }
        return $this->render('UlysseBackBundle:Article:create.html.twig', array('entity' => $image, 'form'   => $form->createView(),));
    }
    
    /**
     * Finds and displays a Article entity.
     *
     * @Route("/{id}", name="article_show")
     * @Method("GET")
     * @Template()
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Article')->find($id);
        $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy( array('article' => $entity));
        return array('entity'      => $entity, 'ventes' => $ventes);
    }
    
/**
     * Edits an existing Article entity.
     *
     * @Route("/{id}", name="back_article_update")
     * @Method("PUT")
     * @Template("UlysseBackBundle:Article:update.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('UlysseBackBundle:Article')->find($id);
        $entity = $em->getRepository('UlysseBackBundle:Image')->findOneByArticle($article);
        
         //-- on recupérè l'admin connecté
        $admin = $this->get('security.context')->getToken()->getUser()->getRoles();
        $role = $this->get('security.context')->isGranted('ROLE_ADMIN');
            
        if(!$entity) {
            $form = $this->createForm(new ArticleType($role), $article, array('action' => $this->generateUrl('back_article_update', array('id' => $id)),
                                                                                                        'method' => 'PUT',));
            $a = 'entity';
        }
        else {
            $form = $this->createForm(new ImageType($role), $entity, array('action' => $this->generateUrl('back_article_update', array('id' => $id)),
                                                                      'method' => 'POST',));
            $a = 'article';
        }
        
        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_article_detail', array('id' => $id)));
        }
       return $this->render('UlysseBackBundle:Article:update.html.twig', array('entity' => $entity, 'entity_id' => $id ,'form'   => $form->createView(),));
    }
    
    /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="back_article_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $article = $em->getRepository('UlysseBackBundle:Article')->find($id);
        $entity = $em->getRepository('UlysseBackBundle:Image')->findOneByArticle($article);
        $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy(array('article' => $id));
        
        if ($ventes)
        {
            return $this->redirect($this->generateUrl('back_article_deactivate', array('id' => $id)));
        } else {
            if (!$entity) {
                $em->remove($article);
                $em->flush();
            }
            else{
                $em->remove($entity);
                $em->flush();
            } 
            return $this->redirect($this->generateUrl('back_articles'));
        }
    }
    
    /**
     * Deactivate Action.
     *
     * @Route("/{id}", name="deactivate")
     * @Method("DELETE")
     */
    public function deactivateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('UlysseBackBundle:Article')->find($id);
        
        if ($article->isWaitingForValidation())
            $article->setRefused($this->get('security.context')->getToken()->getUser());
        elseif ($article->isValidated())
            $article->deactivate();
        
        $em->persist($article);
        $em->flush();
        
        return $this->redirect($this->generateUrl('back_articles'));
    }
    
    /**
     * Activate a Article entity.
     *
     * @Route("/{id}", name="activate")
     * @Method("DELETE")
     */
    public function activateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('UlysseBackBundle:Article')->find($id);
        
        if ($article->isWaitingForValidation())
            $article->setValidated($this->get('security.context')->getToken()->getUser());
        elseif ($article->isValidated())
            $article->activate();

        $em->persist($article);
        $em->flush();
        
        return $this->redirect($this->generateUrl('back_articles'));
    }
    
}
