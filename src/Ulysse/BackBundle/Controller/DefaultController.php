<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $date_du_jour = new \DateTime('now');
        //-- recherche article qui n'ont pas encore été validé
        $articles = $em->createQuery("SELECT A FROM UlysseBackBundle:Article A WHERE A.date_validation LIKE :date")
                        ->setParameter('date', '0000-00-00 00:00:00')->getResult();
        
        //-- recherche des commandes de la journée
        $commandes = $em->createQuery("SELECT C FROM UlysseFrontBundle:Commande C WHERE C.date LIKE :date")
                        ->setParameter('date', '%'.$date_du_jour->format('Y-m-d').'%')->getResult();
       
        //-- recherche des ventes de la journée
        $ventes = $em->createQuery("SELECT V FROM UlysseFrontBundle:Vente V WHERE V.date LIKE :date")
                     ->setParameter('date', '%'.$date_du_jour->format('Y-m-d').'%')->getResult();

        return $this->render('UlysseBackBundle:Default:index.html.twig', 
                             array('articles' => $articles, 'commandes' => $commandes, 'ventes' => $ventes));
    }
    
    //-------------------------- CATEGORIE -------------------------------------
    
    public function subCategeriesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseBackBundle:Sous_categorie')->findAll();

        return $this->render('UlysseBackBundle:Default:subCategeries.html.twig', array('subCategeries' => $entities));
    }
    
    public function detailSubCategeryAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Sous_categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        return $this->render('UlysseBackBundle:Default:detailSubCategery.html.twig', array('subCateger' => $entity));
        //return array('entity'      => $entity,'delete_form' => $deleteForm->createView(),);
    }
    
    public function updateSubCategeryAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Sous_categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubCategery entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_sub_categories'));
        }
        
        return $this->render('UlysseBackBundle:Default:updateSubCategery.html.twig', array('entity'      => $entity,'edit_form'   => $editForm->createView(),'delete_form' => $deleteForm->createView(),));
        //return array('entity'      => $entity,'edit_form'   => $editForm->createView(),'delete_form' => $deleteForm->createView(),);
    }
    
    private function deleteSubCategery($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('back_sub_category_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    //--------------------------- COMMANDE -------------------------------------
    
     public function commandsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseBackBundle:Commande')->findAll();

        return $this->render('UlysseBackBundle:Default:commands.html.twig', array('commandes' => $entities));
    }

    public function detailCommandAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Commande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        return $this->render('UlysseBackBundle:Default:detailCommand.html.twig', array('commande' => $entity));
        //return array('entity'      => $entity,'delete_form' => $deleteForm->createView(),);
    }
    
    //--------------------------- VENTE ----------------------------------------
    
     public function ventesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseBackBundle:Vente')->findAll();

        return $this->render('UlysseBackBundle:Default:ventes.html.twig', array('commandes' => $entities));
    }

    public function detailVenteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Commande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        return $this->render('UlysseBackBundle:Default:detailVente.html.twig', array('commande' => $entity));
        //return array('entity'      => $entity,'delete_form' => $deleteForm->createView(),);
    }
}
