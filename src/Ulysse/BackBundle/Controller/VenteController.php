<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\FrontBundle\Entity\Vente;
use Ulysse\FrontBundle\Form\CommandeType;

/**
 * Commande controller.
 *
 * @Route("/commande")
 */
class VenteController extends Controller
{

    /**
     * Lists all Commande entities.
     *
     * @Route("/", name="back_sales")
     * @Method("GET")
     * @Template("UlysseBackBundle:Vente:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UlysseFrontBundle:Vente')->findAll();
        
        return array('entities' => $entities,);
    }
    
    /**
     * Finds and displays a Commande entity.
     *
     * @Route("/{id}", name="back_sale_detail")
     * @Method("GET")
     * @Template("UlysseBackBundle:Vente:detail.html.twig")
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Vente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Vente entity.');
        }
        return array('entity' => $entity,);
    }
}
