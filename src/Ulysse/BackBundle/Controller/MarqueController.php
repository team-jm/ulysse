<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\BackBundle\Entity\Marque;
use Ulysse\BackBundle\Form\MarqueType;

/**
 * Marque controller.
 *
 * @Route("/marque")
 */
class MarqueController extends Controller
{

    /**
     * Lists all Marque entities.
     *
     * @Route("/", name="back_brands")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseBackBundle:Marque')->findAll();

        return array('entities' => $entities,);
    }
    /**
     * Creates a new Marque entity.
     *
     * @Route("/", name="marque_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:Marque:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Marque();
        $form = $this->createForm(new MarqueType(), $entity, array('action' => $this->generateUrl('back_brand_create'),
                                                                    'method' => 'POST',));

        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('back_brand_detail', array('id' => $entity->getId())));
        }
        
       return $this->render('UlysseBackBundle:Marque:create.html.twig',  array('entity' => $entity,'form'   => $form->createView(),));
    }

    /**
     * Finds and displays a Marque entity.
     *
     * @Route("/{id}", name="back_brand_detail")
     * @Method("GET")
     * @Template()
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UlysseBackBundle:Marque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Marque entity.');
        }

        return array('entity'=> $entity,);
    }

    /**
     * Edits an existing Marque entity.
     *
     * @Route("/{id}", name="marque_update")
     * @Method("PUT")
     * @Template("UlysseBackBundle:Marque:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseBackBundle:Marque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Marque entity.');
        }
        
        $editForm = $this->createForm(new MarqueType(), $entity, array('action' => $this->generateUrl('back_brand_update', array('id' => $entity->getId())),
                                                                        'method' => 'PUT', ));

        $editForm->add('submit', 'submit', array('label' => 'Update'));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_brand_detail', array('id' => $id)));
        }

      return $this->render('UlysseBackBundle:Marque:update.html.twig',  array('entity' => $entity,'form'   => $editForm->createView(),));
    }
    /**
     * Deletes a Marque entity.
     *
     * @Route("/{id}", name="marque_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
       $em = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('UlysseBackBundle:Marque')->find($id);

       $em->remove($entity);
       $em->flush();

        return $this->redirect($this->generateUrl('back_brands'));
    }
}
