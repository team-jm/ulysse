<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\FrontBundle\Entity\Commande;
use Ulysse\FrontBundle\Form\CommandeType;

/**
 * Commande controller.
 *
 * @Route("/commande")
 */
class CommandeController extends Controller
{

    /**
     * Lists all Commande entities.
     *
     * @Route("/", name="back_commands")
     * @Method("GET")
     * @Template("UlysseBackBundle:Commande:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes_ventes = $em->getRepository('UlysseFrontBundle:Commande_Vente')->findAll();
        return array('commandes_ventes' => $commandes_ventes,);
    }
    /**
     * Finds and displays a Commande entity.
     *
     * @Route("/{id}", name="back_command_detail")
     * @Method("GET")
     * @Template("UlysseBackBundle:Commande:detail.html.twig")
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $commande = $em->getRepository('UlysseFrontBundle:Commande')->find($id);

        $commande_ventes = $em->getRepository('UlysseFrontBundle:Commande_Vente')->findBy(array('commande' => $commande));
        
        return array('commande_ventes' => $commande_ventes, 'commande' => $commande);
    }
}
