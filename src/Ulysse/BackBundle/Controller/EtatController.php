<?php

namespace Ulysse\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\FrontBundle\Entity\Etat;
use Ulysse\FrontBundle\Form\EtatType;

/**
 * Etat controller.
 *
 * @Route("/etat")
 */
class EtatController extends Controller
{

    /**
     * Lists all Etat entities.
     *
     * @Route("/", name="back_status")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UlysseFrontBundle:Etat')->findAll();

        return array('entities' => $entities);
    }
    
    /**
     * Creates a new Etat entity.
     *
     * @Route("/", name="back_status_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:Etat:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Etat();
        $form = $this->createForm(new EtatType(), $entity, array('action' => $this->generateUrl('back_status_create'),
                                                                 'method' => 'POST',));
        $form->add('submit', 'submit', array('label' => 'Create'));
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('back_status_detail', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form'   => $form->createView(),);
    }
    /**
     * Finds and displays a Etat entity.
     *
     * @Route("/{id}", name="etat_show")
     * @Method("GET")
     * @Template()
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UlysseFrontBundle:Etat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Etat entity.');
        }
        
        return array('entity' => $entity);
    }
    
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Etat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Etat entity.');
        }
        
        $editForm = $this->createForm(new EtatType(), $entity, array('action' => $this->generateUrl('back_status_update', array('id' => $entity->getId())),
                                                                     'method' => 'PUT',));
        $editForm->add('submit', 'submit', array('label' => 'Update'));

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('back_status_detail', array('id' => $id)));
        }

        return $this->render('UlysseBackBundle:Etat:update.html.twig', array('entity' => $entity,'form'   => $editForm->createView()));
    }
    /**
     * Deletes a Etat entity.
     *
     * @Route("/{id}", name="back_status_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UlysseFrontBundle:Etat')->find($id);

        if (!$entity) {
          throw $this->createNotFoundException('Unable to find Etat entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('back_status'));
    }
}
