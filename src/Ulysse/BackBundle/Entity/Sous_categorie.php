<?php

namespace Ulysse\BackBundle\Entity;

use Symfony\Component\Validator\Constraints as Validate;
use Doctrine\ORM\Mapping as ORM;

/**
 * Sous_categorie
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ulysse\BackBundle\Entity\Sous_categorieRepository")
 */
class Sous_categorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;
    
     /**
     * @var Categorie
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\BackBundle\Entity\Categorie", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $categorie;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Sous_categorie
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    
    /**
     * Set categorie
     * 
     * @param  \Ulysse\BackBundle\Entity\Categorie $categorie
     * @return \Ulysse\BackBundle\Entity\Sous_categorie
     */
    public function setCategorie(Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     * 
     * @return \Ulysse\BackBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
