<?php

namespace Ulysse\BackBundle\Entity;

use Symfony\Component\Validator\Constraints as Validate;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Ulysse\UserBundle\Entity\Utilisateur;

/**
 * Article
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ulysse\BackBundle\Entity\ArticleRepository")
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

     /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
     private $description;
     /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean")
     */
     private $actif;
     
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
     private $date_creation;
     
     /**
     * @var Utilisateur
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\UserBundle\Entity\Utilisateur", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $createur;
    
     /**
     * @var Utilisateur
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\UserBundle\Entity\Utilisateur", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $admin;
     
     /**
     * @var Marque
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\BackBundle\Entity\Marque", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $marque;
     
     /**
     * @var Tag
     * 
     * @ORM\ManyToMany(targetEntity="Ulysse\BackBundle\Entity\Tag", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $tag;
     
     /**
     * @var Sous_categorie
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\BackBundle\Entity\Sous_categorie", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $sous_categorie;
     
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_validation", type="datetime")
     */
     private $date_validation;
     
     /**
     * @ORM\OneToMany(targetEntity="Ulysse\BackBundle\Entity\Image", mappedBy="article", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
     private $images;
     
     private $not_validated;
     
     const ACTIF = true;
     const INACTIF = false;
     
     public function __construct()
    {         
      $this->tag = new ArrayCollection();
      $this->images = new ArrayCollection();
      $this->not_validated = new \DateTime('0000-00-00 00:00:00');
      $this->setWaiting();
      
    }
    
    public function setValided($admin)
    {
       $this->activate();
       $this->setAdmin($admin);
       $this->date_validation = new \DateTime();
       return $this;
    }
    
     public function setRefused($admin)
     {
         $this->deactivate();
         $this->setAdmin($admin);
         $this->date_validation = new \DateTime();
         return $this;
     }

    public function setWaiting()
     {
         $this->deactivate();
         $this->setDateValidation($this->not_validated);
         return $this;
     }
     
     /**
      * Is Valid ?
      * 
      * @return boolean
      */
     public function isValidated()
     {
         return ($this->date_validation != $this->not_validated);
     }
     
    /**
      * Is Waiting For Validation by Admin ?
      * 
      * @return boolean
      */
     public function isWaitingForValidation()
     {
         return ($this->date_validation == $this->not_validated);
     }      

    /**
      * Is Not Validated by Admin ?
      * 
      * @return boolean
      */
     public function isNotValidated()
     {
         return ($this->date_validation == $this->not_validated && $this->actif ==self::INACTIF);
     }      

    /**
      * Is Refused by Admin ?
      * 
      * @return boolean
      */
     public function isRefused()
     {
         return ($this->date_validation != $this->not_validated && $this->actif == self::INACTIF);
     }      
     
     public function deactivate()
     {
         $this->setActif(self::INACTIF);
     }
     public function activate()
     {
         $this->setActif(self::ACTIF);
     }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Article
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Article
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Get date_validation
     *
     * @return \DateTime 
     */
     public function getDateValidation() {
         return $this->date_validation;
     }

          /**
     * Set date_validation
     *
     * @param \DateTime $date_validation
     * @return Article
     */
     public function setDateValidation(\DateTime $date_validation) {
         $this->date_validation = $date_validation;
         return $this;
     }
    
     /**
     * Set date_creation
     *
     * @param \DateTime $date_creation
     * @return Article
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Get date_creation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }
    
     /**
     * Set createur
     * 
     * @param  \Ulysse\UserBundle\Entity\Utilisateur $createur
     * @return \Ulysse\UserBundle\Entity\Article
     */
    public function setCreateur(Utilisateur $createur)
    {
        $this->createur = $createur;
        return $this;
    }

    /**
     * Get createur
     * 
     * @return \Ulysse\FrontBundle\Entity\Utilisateur
     */
    public function getCreateur()
    {
        return $this->createur;
    }
    
    /**
     * Set createur
     * 
     * @param  \Ulysse\UserBundle\Entity\Utilisateur $admin
     * @return \Ulysse\UserBundle\Entity\Article
     */
     public function setAdmin(Utilisateur $admin)
     {
       $this->admin = $admin;
       return $this;
     }

    /**
     * Get createur
     * 
     * @return \Ulysse\FrontBundle\Entity\Utilisateur
     */
    public function getAdmin()
    {
        return $this->admin;
    }
    
    /**
     * Set marque
     * 
     * @param  \Ulysse\BackBundle\Entity\Marque $marque
     * @return \Ulysse\BackBundle\Entity\Article
     */
    public function setMarque(Marque $marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     * 
     * @return \Ulysse\BackBundle\Entity\Marque
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set tag
     * 
     * @param  \Ulysse\GeneralBundle\Entity\Tag $tag
     * @return \Ulysse\GeneralBundle\Entity\Article
     */
    public function addTag(Tag $tag)
    {
        $this->tag[] = $tag;

        return $this;
    }

    /**
     * Get tag
     * 
     * @return \Ulysse\GeneralBundle\Entity\Tag
     */
    public function removeTag(Tag $tag)
    {
        return $this->tag->removeElement($tag);
    }
    
    /**
     * Get tag
     * 
     * @return \Ulysse\GeneralBundle\Entity\Tag
     */
    public function getTag()
    {
        return $this->tag;
    }
    
    /**
     * Set sous_categorie
     * 
     * @param  \Ulysse\BackBundle\Entity\Sous_categorie $sous_categorie
     * @return \Ulysse\BackBundle\Entity\Article
     */
    public function setSousCategorie(Sous_categorie $sous_categorie)
    {
        $this->sous_categorie= $sous_categorie;

        return $this;
    }

    /**
     * Get sous_categorie
     * 
     * @return \Ulysse\BackBundle\Entity\Sous_categorie
     */
    public function getSousCategorie()
    {
        return $this->sous_categorie;
    }
    
    /**
     * Set images
     * 
     * @param  \Ulysse\GeneralBundle\Entity\Image $image
     * @return \Ulysse\GeneralBundle\Entity\Article
     */
    public function addImage(Image $image)
    {
        $image->
        $this->images[] = $image;

        return $this;
    }

    public function removeImage(Image $image)
    {
        $this->images->removeElement($image);
    }

    public function getImages()
    {
        return $this->images;
    }
}
