<?php

namespace Ulysse\BackBundle\Form;

use Ulysse\BackBundle\Entity\Image;
use Ulysse\BackBundle\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ArticleType extends AbstractType
{
    
    private $role;
    
    public function __construct($role = null) {
        $this->role = $role;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
                ->add('description');
                if($this->role && $this->role[0] == 'ROLE_ADMIN'){$builder->add('actif','checkbox', array( 'required'  => false));}
                $builder->add('marque','entity', array('multiple' => false,
                                               'class' => 'Ulysse\BackBundle\Entity\Marque', 
                                               'property' => 'libelle'))
                ->add('tag', 'collection', array('type' => new TagType(),'allow_add' => true,'by_reference' => false,))
                ->add('sous_categorie','entity', array('multiple' => false,'class' => 'Ulysse\BackBundle\Entity\Sous_categorie', 'property' => 'libelle'))
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ulysse\BackBundle\Entity\Article'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ulysse_backbundle_article';
    }
}
