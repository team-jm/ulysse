<?php

namespace Ulysse\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class Sous_categorieType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categorie', 'entity', array(
                'class' => 'Ulysse\BackBundle\Entity\Categorie',
                'property' => 'libelle'))
            ->add('libelle');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ulysse\BackBundle\Entity\Sous_categorie'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ulysse_backbundle_sub_categorie';
    }
}
