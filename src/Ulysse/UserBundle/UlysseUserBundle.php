<?php

namespace Ulysse\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UlysseUserBundle extends Bundle
{
    public function getParent() {
        return 'FOSUserBundle';
    }
}
