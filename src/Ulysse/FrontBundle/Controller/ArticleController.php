<?php

namespace Ulysse\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\BackBundle\Entity\Article;
use Ulysse\BackBundle\Form\ArticleType;
use Ulysse\BackBundle\Entity\Image;
use Ulysse\BackBundle\Form\ImageType;
/**
 * Article controller.
 *
 * @Route("/article")
 */
class ArticleController extends Controller
{
    public function indexAction()
    {
        $result = $this->getActiveAndBeingSoldArticleAndBestPrice();

        return $this->render('UlysseFrontBundle:Article:index.html.twig',array(
            'entities' => $result[0],
            'prices' => $result[1],
        )); 
    }

    public function getFourMostSuccessfulActiveAndBeingSoldArticleAndBestPrice()
    {
        $em = $this->getDoctrine()->getManager();
        $ventes_id = array();
        $ventes_idTmp = $em->getRepository('UlysseFrontBundle:Commande_Vente')->findAllVentesSortedByCommandes();

        foreach ($ventes_idTmp as $vente_id)
        {
            $ventes_id[] = (int) $vente_id[1];
        }

        $articles = array();
        $prices = array();

        foreach ($ventes_id as $vente_id)
        {
            if (count($articles) <4)
            {
                if ($vente = $em->getRepository('UlysseFrontBundle:Vente')->find($vente_id))
                {
                    if ($article = $em->getRepository('UlysseBackBundle:Article')->find($vente->getArticle()->getId()))
                    {
                        if(!in_array($article,$articles))
                        {
                        if ($ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy(array('article' => $article)))
                        {
                            $articles[] = $article;
                            $bestPrice = $ventes[0]->getPrix();
                            foreach ($ventes as $vente)
                            {
                                $bestPrice = ($vente->getPrix() < $bestPrice) ? $vente->getPrix() : $bestPrice;
                            }
                            $prices[$article->getId()] = array($vente->getId(),$bestPrice);
                        } }}}}}

        return array($articles,$prices);
    }
    
    public function getFourMostRecentActiveAndBeingSoldArticleAndBestPrice()
    {
        $em = $this->getDoctrine()->getManager();
        $articlesTmp = $em->getRepository('UlysseBackBundle:Article')->findAllSortByCreationDate();
        $articles = array();
        $prices = array();
        foreach ($articlesTmp as $article)
        {
            if (count($articles) <4 && $article->getActif())
            {
                $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy(array('article' => $article));
                if ($ventes)
                {
                    $articles[] = $article;
                    $bestPrice = $ventes[0]->getPrix();
                    foreach ($ventes as $vente)
                    {
                        $bestPrice = ($vente->getPrix() < $bestPrice) ? $vente->getPrix() : $bestPrice;
                    }
                    $prices[$article->getId()] = array($vente->getId(),$bestPrice);
                }
            }
        }
        unset($articlesTmp);
        return array($articles,$prices);
    }
    
    public function getActiveAndBeingSoldArticleAndBestPrice()
    {
        $em = $this->getDoctrine()->getManager();
        $articlesTmp = $em->getRepository('UlysseBackBundle:Article')->findAll();
        $articles = array();
        $prices = array();
        foreach ($articlesTmp as $article)
        {
            if ($article->getActif())
            {
                $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy(array('article' => $article));
                if ($ventes)
                {

                    $articles[] = $article;
                    $bestPrice = $ventes[0]->getPrix();
                    $venteBestPrice = $ventes[0]->getId();
                    foreach ($ventes as $vente)
                    {
                        if (($vente->getPrix() < $bestPrice))
                        {
                            $bestPrice = $vente->getPrix();
                            $venteBestPrice = $vente->getId();
                        }
                    }
                    $prices[$article->getId()] = array($venteBestPrice,$bestPrice);
                }
            }
        }
        unset($articlesTmp);
        return array($articles,$prices);
    }
    
    public function homePreviewBestSalesAction()
    {
        $result = $this->getFourMostSuccessfulActiveAndBeingSoldArticleAndBestPrice();

        return $this->render('UlysseFrontBundle:Article:homepreview.html.twig',array(
            'articles' => $result[0],
            'prices' => $result[1],
        )); 
    }
    
    public function homePreviewMostRecentAction()
    {
        
        $result = $this->getFourMostRecentActiveAndBeingSoldArticleAndBestPrice();

        return $this->render('UlysseFrontBundle:Article:homepreview.html.twig',array(
            'articles' => $result[0],
            'prices' => $result[1],
        )); 
    }
    
    /**
     * Creates a new Article entity.
     *
     * @Route("/", name="article_create")
     * @Method("POST")
     * @Template("UlysseBackBundle:Article:new.html.twig")
     */
    public function createAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            return $this->redirect($this->generateUrl('front_list_article'));
        }
        
        $article = new Article();
        $image = new Image();
        $image->setArticle($article); 

        $user = $this->get('security.context')->getToken()->getUser();
        $role = $this->get('security.context')->getToken()->getUser()->getRoles();
        $form = $this->createForm(new ImageType($role), $image, array('action' => $this->generateUrl('front_create_article'),
                                                                       'method' => 'POST',));
        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);
        
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $image->getArticle()->setCreateur($user);
            $image->getArticle()->setActif(false);
            //- on y intègre la date de création actuelle
            $date_creation = new \DateTime();
            $image->getArticle()->setDateCreation($date_creation);
            
            
            $em->persist($image);
            $em->flush();
            
            $this->addFlash(
                'success',
                'Votre article a été enregistré et est en cours de validation!'
            );
            
            return $this->redirect($this->generateUrl('front_list_article'));
        }
        return $this->render('UlysseFrontBundle:Article:create.html.twig', array('entity' => $image, 'form'   => $form->createView(),));
    }
    
    /**
     * Find articles from this search
     * 
     * @return Article
     */
    public function findAction(){
        $search = $this->getRequest()->get('search');

        $em       = $this->getDoctrine()->getManager();
        //-- requête qui recherche les articles dont le nom contient la valeur rechercher
        $query    = $em->createQuery("SELECT A FROM UlysseBackBundle:Article A WHERE A.nom LIKE :search")->setParameter('search', '%'.$search.'%');
        $articlesTmp = $query->getResult();
        
        //-- Tableau qui contiendra le plus petit prix d'une vente par article
        $prices = array();
        
        foreach ($articlesTmp as $article) {
            $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy( array('article' => $article),array('prix' => 'ASC'));
            if($ventes){
                $prices[$article->getId()] = array($ventes[0]->getId(), $ventes[0]->getPrix());
                $articles[] = $article;
            }
        }
        
        return $this->render('UlysseFrontBundle:Article:find.html.twig',array(
            'entities' => $articles,
            'prices' => $prices,
        )); 
    }
    
    /**
     * Find articles by  Sous_categorie
     * 
     * @return Article
     */
    public function findBySousCategorieAction($id){
        
        $em       = $this->getDoctrine()->getManager();
        
        if (!$sous_categorie = $em->getRepository('UlysseBackBundle:Sous_categorie')->find($id))
        {
            $this->addFlash('notice', 'Sous-catégorie non trouvée');
            return $this->redirect($this->generateUrl('front_list_article'));
        }
        $articlesTmp = $em->getRepository('UlysseBackBundle:Article')->findBy(array('sous_categorie' => $sous_categorie));
        
        //-- Tableau qui contiendra le plus petit prix d'une vente par article
        $prices = array();
        $articles = array();
        
        foreach ($articlesTmp as $article) {
            $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy( array('article' => $article),array('prix' => 'ASC'));
            if($ventes){
                $prices[$article->getId()] = array($ventes[0]->getId(), $ventes[0]->getPrix());
                $articles[] = $article;
            }
        }
        
        return $this->render('UlysseFrontBundle:Article:find.html.twig',array(
            'entities' => $articles,
            'prices' => $prices,
            'sous_categorie' => $sous_categorie->getLibelle() )); 
    }
    
}
