<?php

namespace Ulysse\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\FrontBundle\Entity\Commande;
use Ulysse\UserBundle\Entity\Utilisateur;
use Ulysse\FrontBundle\Form\CommandeType;
use Ulysse\FrontBundle\Cart\Panier;
use Ulysse\FrontBundle\Entity\Commande_Vente;

/**
 * Commande controller.
 *
 * @Route("/commande")
 */
class CommandeController extends Controller
{

    /**
     * Lists all Commande entities.
     *
     * @Route("/", name="commande")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseFrontBundle:Commande')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    public function initPanier()
    {
        if (!Panier::$em)
            Panier::$em = $this->getDoctrine()->getEntityManager();
        return Panier::initPanier($this->get('security.context')->getToken()->getUser());
    }
    public function cartAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            if(!$url = $request->headers->get('referer'))
                    $url = $this->generateUrl('front_homepage');
            return $this->redirect($url);
        }
        
        if ($this->initPanier())
        {
            return $this->render('UlysseFrontBundle:Cart:panier.html.twig', array(
                'ventesArray' => Panier::listVentes(),
            ));
        }
    }
    
    public function emptyCartAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            if(!$url = $request->headers->get('referer'))
                $url = $this->generateUrl('front_homepage');
            return $this->redirect($url);
        }
        
        Panier::emptyAll();
        $this->addFlash(
            'success',
            "Votre panier a bien été réinitialisé."
        );
        return $this->redirect($this->generateUrl('front_cart'));
    }
    
    public function removeCartAction($id_vente, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            if(!$url = $request->headers->get('referer'))
                $url = $this->generateUrl('front_homepage');
            return $this->redirect($url);
        }
        
        if ($this->initPanier())
        {
            $em = $this->getDoctrine()->getManager();
            $vente = $em->getRepository('UlysseFrontBundle:Vente')->find($id_vente);

            if ($vente)
            {
                Panier::removeVente($vente);

                $this->addFlash(
                    'success',
                    "L'article a bien été supprimé du panier!"
                );
                return $this->redirect($this->generateUrl('front_cart'));
            }
            $this->addFlash(
                'notice',
                "Une erreur est survenue lors de la suppression de l'article du panier."
            );
            return $this->redirect($this->generateUrl('front_cart'));
        }
    }
    
    public function addCartAction($id_vente, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            if(!$url = $request->headers->get('referer'))
            {
                $em = $this->getDoctrine()->getManager();
                $vente = $em->getRepository('UlysseFrontBundle:Vente')->find($id_vente);
                if ($id = $vente->getId())
                {
                    $url = $this->generateUrl('front_detail_article', array(
                        'id' => $id
                    ));
                } else {
                    $url = $this->generateUrl('front_homepage');
                }
            }
            return $this->redirect($url);
        }
        
        if ($this->initPanier())
        {
            $em = $this->getDoctrine()->getManager();
            $vente = $em->getRepository('UlysseFrontBundle:Vente')->find($id_vente);

            if ($vente)
            {
                if ((!isset($_POST['quantite']))
                        || (isset($_POST['quantite'])
                                && !$quantite = htmlspecialchars((int) $_POST['quantite']))) {
                    $quantite = 1;
                }
                
                if (!Panier::addVente($vente, $quantite))
                {
                    $this->addFlash(
                        'notice',
                        "Une erreur est survenue lors de l'ajout de la vente au panier!"
                    );
                    return $this->redirect($this->generateUrl('front_homepage'));
                } else {
                    $this->addFlash(
                        'success',
                        "L'article a bien été ajouté au panier!"
                    );
                    return $this->redirect($this->generateUrl('front_detail_article', array(
                        'id' => $vente->getId(),
                    )));
                }
            }
            $this->addFlash(
                'notice',
                "Une erreur est survenue lors de l'ajout de la vente au panier!"
            );
            return $this->redirect($this->generateUrl('front_homepage'));
        }
    }
    public function addVente($commande, $vente, $quantite)
    {
        if ($vente && $commande)
        {
            $em = $this->getDoctrine()->getManager();
            if ($vente->getStock() >= $quantite)
            {
                if(! $em->getRepository('UlysseFrontBundle:Commande_Vente')->findBy(array('commande' => $commande, 'vente' => $vente)))
                {
                    $commandeVente = new Commande_Vente($commande, $vente, $quantite);
                    $vente->setStock($vente->getStock()-$quantite);
                    if ($vente->getStock() == 0)
                        $vente->desactivate();
                    
                    $em->persist($commandeVente);
                    $em->persist($vente);
                    $em->flush();
                }
                return true;
            }
            return false;
        }
        return false;
    }
    
    public function navBarCartAction()
    {
        if ($this->initPanier())
        {
            $total = Panier::getTotalPrice();
            $nbventes = Panier::getVenteNumber();
        }
        else
        {
            $total = 0;
            $nbventes = 0;
        }
            return $this->render('UlysseFrontBundle:NavBar:cart.html.twig', array(
                'total' => $total,
                'nbventes' => $nbventes,
            ));
    }
    public function createAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {

            if(!$url = $request->headers->get('referer'))
            {
                    $url = $this->generateUrl ('front_cart');
            } else {
                $this->addFlash(
                  'notice',
                  'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
                );
            }
            return $this->redirect($url);
        }
        
        if ($this->initPanier())
        {
            $user  = $this->get('security.context')->getToken()->getUser();
            $em = $this->getDoctrine()->getManager();  
            if ($user && $ventesArray = Panier::getAllVentes($user))
            {   
                foreach ($ventesArray as $venteArray)
                {
                   $vente = $venteArray['vente'][0];
                   $quantite = $venteArray['quantite'];
                   if ($vente->getStock() < $quantite)
                   {
                        $this->addFlash(
                            'notice',
                            "Il n'y a plus assez d'unités disponibles pour l'article ".$vente->getArticle()->getNom()."."
                        );
                        return $this->redirect($this->generateUrl('front_cart'));
                   }
                }
                $total = 0.0;
                $commande = new Commande($user);
                $em->persist($commande);
                $em->flush();
                foreach ($ventesArray as $venteArray)
                {
                    $vente = $venteArray['vente'][0];
                    $quantite = $venteArray['quantite'];
                    if (!$this->addVente($commande, $vente, $quantite))
                    {
                        $commande->setPrix($total);

                        $em->persist($commande);
                        $em->flush();
                        $this->addFlash(
                            'notice',
                            "Votre commande n'a pas été passée."
                        );
                        return $this->redirect($this->generateUrl('front_cart'));
                    }
                    Panier::removeVente($vente, $user);
                    $total += ((float) $quantite) * $vente->getPrix();
                }
                $commande->setPrix($total);

                $em->persist($commande);
                $em->flush();

                $this->addFlash(
                    'success',
                    "Vos commandes ont bien été enregistrées!"
                );
                
                return $this->redirect($this->generateUrl('front_cart'));
            } else {
                
                $this->addFlash(
                    'notice',
                    "Une erreur a été rencontrée au cours de la validation de votre panier"
                );
                
                return $this->redirect($this->generateUrl('front_cart'));
            }
        }
    }

    /**
     * Creates a form to create a Commande entity.
     *
     * @param Commande $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Commande $entity)
    {
        $form = $this->createForm(new CommandeType(), $entity, array(
            'action' => $this->generateUrl('commande_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Commande entity.
     *
     * @Route("/new", name="commande_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Commande();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Commande entity.
     *
     * @Route("/{id}", name="commande_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Commande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Commande entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Commande entity.
     *
     * @Route("/{id}/edit", name="commande_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Commande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Commande entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Commande entity.
    *
    * @param Commande $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Commande $entity)
    {
        $form = $this->createForm(new CommandeType(), $entity, array(
            'action' => $this->generateUrl('commande_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Commande entity.
     *
     * @Route("/{id}", name="commande_update")
     * @Method("PUT")
     * @Template("UlysseFrontBundle:Commande:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Commande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Commande entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('commande_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Commande entity.
     *
     * @Route("/{id}", name="commande_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UlysseFrontBundle:Commande')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Commande entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('commande'));
    }

    /**
     * Creates a form to delete a Commande entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commande_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
