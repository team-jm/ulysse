<?php

namespace Ulysse\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ulysse\FrontBundle\Entity\Vente;
use Ulysse\FrontBundle\Form\VenteType;

/**
 * Vente controller.
 *
 * @Route("/vente")
 */
class VenteController extends Controller
{
    public function detailVendeurAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$vendeur = $em->getRepository('UlysseUserBundle:Utilisateur')->find($id))
        {
            $this->addFlash('notice', "Le vendeur recherché n'existe pas");
            $this->redirect($this->generateUrl('front_homepage'));
        }
        $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findByVendeur($vendeur);

        return $this->render('UlysseFrontBundle:Vente:detail_vendeur.html.twig', array(
            'vendeur' => $vendeur,
            'ventes' => $ventes
        ));
    }
    
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $vente = $em->getRepository('UlysseFrontBundle:Vente')->find($id);
        
        if (!$vente) {
            throw $this->createNotFoundException('Unable to find Vente entity.');
        }

        $ventes = $em->getRepository('UlysseFrontBundle:Vente')->findBy(array('article' => $vente->getArticle()->getId()));

        return $this->render('UlysseFrontBundle:Vente:detail.html.twig', array(
            'vente' => $vente,
            'ventes' => $ventes
        ));
    }
    /**
     * Lists all Vente entities.
     *
     * @Route("/", name="vente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UlysseFrontBundle:Vente')->findAll();

        return $this->render('UlysseFrontBundle:Vente:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Vente entity.
     *
     * @Route("/", name="vente_create")
     * @Method("POST")
     * @Template("UlysseFrontBundle:Vente:new.html.twig")
     */
    public function createAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            if(!$url = $request->headers->get('referer'))
                    $url = $this->generateUrl ('front_list_article');
            return $this->redirect($url);
        }
        if (!$preSelectedArticle = $request->get('article_id'))
        {
            $preSelectedArticle = '';
        }
        $entity = new Vente();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setVendeur($this->get('security.context')->getToken()->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre vente a bien été ajoutée! Merci beaucoup!'
            );
            
            return $this->redirect($this->generateUrl('front_detail_article', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'preselected_article' => $preSelectedArticle,
        );
    }

    /**
     * Creates a form to create a Vente entity.
     *
     * @param Vente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Vente $entity)
    {
        $form = $this->createForm(new VenteType(), $entity, array(
            'action' => $this->generateUrl('front_add_sale'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Vente entity.
     *
     * @Route("/new", name="vente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Vente();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Vente entity.
     *
     * @Route("/{id}/edit", name="vente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Vente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Vente entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Vente entity.
    *
    * @param Vente $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Vente $entity)
    {
        $form = $this->createForm(new VenteType(), $entity, array(
            'action' => $this->generateUrl('vente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Vente entity.
     *
     * @Route("/{id}", name="vente_update")
     * @Method("PUT")
     * @Template("UlysseFrontBundle:Vente:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UlysseFrontBundle:Vente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Vente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('vente_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Vente entity.
     *
     * @Route("/{id}", name="vente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UlysseFrontBundle:Vente')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Vente entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('vente'));
    }

    /**
     * Creates a form to delete a Vente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
