<?php

namespace Ulysse\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if (!$user = $this->get('security.context')->getToken()->getUser())
                $user = new Utilisateur();
        
        $articles = $em->getRepository('UlysseBackBundle:Article')->findAll();
        
        return $this->render('UlysseFrontBundle:Default:index.html.twig', array(
            'user' => $user,
            'articles' => $articles,
        ));
    }
    
    public function contactAction()
    {
        $user= $this->get('security.context')->getToken()->getUser();
        $post = Request::createFromGlobals();

        if ($post->request->has('submit')) {
            $email = $post->request->get('userEmail');
            $subject = $post->request->get('userSubject');
            $message = $post->request->get('userMsg');

            if ($subject && $email && $message) {
                $moderateurMail = 'jeff.ipformationv2@gmail.com';
                $mail = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($email)
                    ->setTo($moderateurMail)
                    ->setBody(
                        $this->renderView(
                            'UlysseFrontBundle:Mail:contact.html.twig',
                            array(
                                'message' => $message
                            )
                        ),
                        'text/html'
                    );

                $result = $this->get('mailer')->send($mail);

                if ($result)
                {
                    $this->addFlash(
                        'notice',
                        "Votre message a bien été envoyé!"
                    );
                } else {
                    $this->addFlash(
                        'notice',
                        "Echec de l'envoi du message, veuillez recommencer une nouvelle fois."
                    );  
                }
            } else {
                $this->addFlash(
                    'notice',
                    "Vous n'avez pas précisé tous les champs, veuillez recommencer s'il vous plaît."
                );
            }
        }
        return $this->render('UlysseFrontBundle:Default:contact.html.twig', array(
             'user' => $user, 
        ));
    }
    
    public function contactAdminAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $this->addFlash(
                'notice',
                'Vous devez être connecté pour avoir accès à cette fonctionnalité.'
            );
            if(!$url = $request->headers->get('referer'))
                    $url = $this->generateUrl ('front_homepage');
            return $this->redirect($url);
        }
        
        $user= $this->get('security.context')->getToken()->getUser();
        $post = Request::createFromGlobals();
        if ($post->request->has('submit')) {
            $email = $post->request->get('email');
            $subject = $post->request->get('subject');
            $message = $post->request->get('message');
            
            if ($subject && $email && $message) {
                $adminMail = 'jeff.ipformationv2@gmail.com';
                $mail = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($email)
                    ->setTo($adminMail)
                    ->setBody(
                        $this->renderView(
                            'UlysseFrontBundle:Mail:contact_admin.html.twig',
                            array(
                                'message' => $message
                            )
                        ),
                        'text/html'
                    );
                if ($this->get('mailer')->send($mail))
                {
                    $this->addFlash(
                        'notice',
                        "Votre message a bien été envoyé!"
                    );
                } else {
                    $this->addFlash(
                        'notice',
                        "Echec de l'envoi du message, veuillez recommencer une nouvelle fois."
                    );  
                }
            } else {
                $this->addFlash(
                    'notice',
                    "Vous n'avez pas précisé tous les champs, veuillez recommencer s'il vous plaît."
                );
            }
        }
        return $this->render('UlysseFrontBundle:Default:contactAdmin.html.twig', array(
             'user' => $user, 
        ));
    }
    
    public function aproposAction()
    {
        return $this->render('UlysseFrontBundle:Default:apropos.html.twig');
    }
}
