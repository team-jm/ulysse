<?php

namespace Ulysse\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class VenteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix', 'integer')
            ->add('stock', 'integer')
            ->add('commentaire', 'text')
            ->add('article', 'entity', array(
                'class' => 'Ulysse\BackBundle\Entity\Article',
                'property' => 'id',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                              ->where('u.actif = 1');
                },
            ))
            ->add('etat', 'entity', array(
                'class' => 'Ulysse\FrontBundle\Entity\Etat',
                'property' => 'libelle'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ulysse\FrontBundle\Entity\Vente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ulysse_frontbundle_vente';
    }
}
