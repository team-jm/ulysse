<?php

namespace Ulysse\FrontBundle\Cart;

use Ulysse\UserBundle\Entity\Utilisateur;
use Ulysse\FrontBundle\Entity\Vente;

class Panier
{
    private $ventesArray;
    private $user;
    static public $em;

    public function __construct($user = null)
    {
        $this->ventesArray = array();
        $this->user = (is_null($user)) ? new Utilisateur() : $user;
    }
    static function listVentes()
    {
        if ($panier = self::getPanier())
        {
            return $panier->getVentesArray();
        }
        return false;
    }
    static function getAllVentes()
    {
        if ($panier = self::getPanier())
            return $panier->getVentesArray();
        return false;
    }
    static function emptyAll()
    {
        $_SESSION['panier'] = null;
    }
    static function addVente(Vente $vente, $quantite = null)
    {                         
        if ($panier = self::getPanier())
        {
            if ($panier->addPublicVente($vente, $quantite))
            {
                self::putPanierToSession($panier);
                return true;
            }
        }
        return false;
    }
    public function addPublicVente(Vente $vente, $quantite = 0)
    {
        if (isset($this->ventesArray[$vente->getId()]))
        {
            $quantite = (int) $this->ventesArray[$vente->getId()]['quantite'] + $quantite;
        }
        if ($vente->getStock() >= $quantite)
        {
            $this->ventesArray[$vente->getId()] = array(
                'vente' => $vente,
                'quantite' => $quantite,
            );
            return true;
        } else {
            return false;
        }
    }
    static function removeVente(Vente $vente)
    {
        if ($panier = self::getPanier())
        {
            $ventesArray = $panier->getVentesArray();
            if (isset($ventesArray[$vente->getId()]))
            {
                $quantite = $ventesArray[$vente->getId()]['quantite'];
                unset($ventesArray[$vente->getId()]);
                $panier->setVentesArray($ventesArray);
                self::putPanierToSession($panier);
            }
        }
    }
    static function getPanier()
    {
        if (self::initPanier())
        {   
            return self::getPanierFromSession();
        }
    }
    static function initPanier($user = null)
    {
        if (!isset($_SESSION['panier']) && empty($_SESSION['panier']))
        {
            if (!($user) || $user === 'anon.')
                return false;
            else {
                $panier = new Panier($user);
                self::putPanierToSession($panier);
            }
        }
        return true;
    }
    static function getPanierFromSession()
    {
        $panier = $_SESSION['panier'];
        $panier = self::hydratePanier($panier);
        return $panier;
    }
    static function getTotalPrice()
    {
        if ($panier = self::getPanier())
        {
            $ventesArray = $panier->getVentesArray();
            $total = 0;
            foreach ($ventesArray as $venteArray)
            {
                $prix = $venteArray['vente'][0]->getPrix();
                $quantite = $venteArray['quantite'];
                $total += $prix*$quantite;
            }
            return $total;
        }
        return false;
    }
    static function getVenteNumber()
    {
        if ($panier = self::getPanier())
        {
            $ventesArray = $panier->getVentesArray();
            return count($ventesArray);
        }
        return false;
    }
    static function hydratePanier($panier)
    {
        if ($panier)
        {
            $ventesArray = $panier->getVentesArray();
            $ids = array_keys($ventesArray);
            foreach ($ids as $id)
                $ventesArray[$id]['vente'] = self::$em->getRepository('UlysseFrontBundle:Vente')->findById($id);      
            $panier->setVentesArray($ventesArray);
            $user = self::$em->getRepository('UlysseUserBundle:Utilisateur')->findById($panier->getUser()->getId())[0];
            $panier->setUser($user);
            return $panier;
        }
        return false;
    }
    static function putPanierToSession($panier)
    {
            $_SESSION['panier'] = $panier;
    }

    public function getVentesArray() {
        return $this->ventesArray;
    }

    public function getUser() {
        return $this->user;
    }

    public function setVentesArray(Array $ventesArray) {
        $this->ventesArray = $ventesArray;
    }

    public function setUser(Utilisateur $user) {
        $this->user = $user;
    }
}