<?php

namespace Ulysse\FrontBundle\Entity;

use Symfony\Component\Validator\Constraints as Validate;
use Doctrine\ORM\Mapping as ORM;
use Ulysse\FrontBundle\Entity\Vente;
use Ulysse\FrontBundle\Entity\Commande;

/**
 * Commande
 *
 * @ORM\Table(name="commande_vente")
 * @ORM\Entity(repositoryClass="Ulysse\FrontBundle\Entity\Commande_VenteRepository")
 */
class Commande_Vente
{
  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
  
  
  /**
   * @ORM\ManyToOne(targetEntity="Ulysse\FrontBundle\Entity\Commande", cascade={"persist"}, fetch="EAGER")
   * @ORM\JoinColumn(nullable=false)
   */
  private $commande;


  /**
   * @ORM\ManyToOne(targetEntity="Ulysse\FrontBundle\Entity\Vente", cascade={"persist"}, fetch="EAGER")
   * @ORM\JoinColumn(nullable=false)
   */
  private $vente;

    /**
   * @ORM\Column(name="quantite", type="integer")
   */
  private $quantite;
  
  public function __construct($commande, $vente, $quantite)
  {
      $this->commande = ($commande) ? $commande : new Commande();
      $this->vente = ($vente) ? $vente : new Vente();
      $this->quantite = ($quantite) ? $quantite : 0;
  }
  
  public function getId() {
      return $this->id;
  }

  public function getCommande() {
      return $this->commande;
  }

  public function getVente() {
      return $this->vente;
  }

  public function getQuantite() {
      return $this->quantite;
  }

  public function setId($id) {
      $this->id = $id;
  }

  public function setCommande(Commande $commande) {
      $this->commande = $commande;
  }

  public function setVente(Vente $vente) {
      $this->vente = $vente;
  }

  public function setQuantite($quantite) {
      $this->quantite = $quantite;
  }
}
