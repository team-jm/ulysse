<?php

namespace Ulysse\FrontBundle\Entity;

use Symfony\Component\Validator\Constraints as Validate;
use Doctrine\ORM\Mapping as ORM;
use Ulysse\UserBundle\Entity\Utilisateur;
use Ulysse\BackBundle\Entity\Article;
/**
 * Vente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ulysse\FrontBundle\Entity\VenteRepository")
 */
class Vente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text")
     */
    private $commentaire;

    /**
     * @var Utilisateur
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\UserBundle\Entity\Utilisateur", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $vendeur;
     
     /**
     * @var Article
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\BackBundle\Entity\Article", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $article;
     
     /**
     * @var Etat
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\FrontBundle\Entity\Etat", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $etat;
    
     /**
      * @var boolean
      * 
      * @ORM\Column(name="actif", type="boolean")
      */
     private $actif;

     const ACTIF = true;
     const INACTIF = false;
     
     public function __construct()
     {
         $this->setDate(new \DateTime());

         $this->setVendeur(new Utilisateur());
         $this->activate();
     }
     
     public function getActif() {
         return $this->actif;
     }

     public function setActif($actif) {
         $this->actif = $actif;
     }
     
     public function desactivate()
     {
         $this->setActif(self::INACTIF);
     }
     public function activate()
     {
         $this->setActif(self::ACTIF);
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Vente
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Vente
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Vente
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     * @return Vente
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }
    
    /**
     * Set vendeur
     * 
     * @param  \Ulysse\UserBundle\Entity\Utilisateur $vendeur
     * @return \Ulysse\UserBundle\Entity\Vente
     */
    public function setVendeur(Utilisateur $vendeur)
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    /**
     * Get vendeur
     * 
     * @return \Ulysse\UserBundle\Entity\Utilisateur
     */
    public function getVendeur()
    {
        return $this->vendeur;
    }
    
    /**
     * Set article
     * 
     * @param  \Ulysse\BackBundle\Entity\Article $article
     * @return \Ulysse\BackBundle\Entity\Vente
     */
    public function setArticle(Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     * 
     * @return \Ulysse\BackBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }
    
    /**
     * Set etat
     * 
     * @param  \Ulysse\FrontBundle\Entity\Etat $etat
     * @return \Ulysse\FrontBundle\Entity\Vente
     */
    public function setEtat(Etat $etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     * 
     * @return \Ulysse\FrontBundle\Entity\Etat
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
