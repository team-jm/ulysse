<?php

namespace Ulysse\FrontBundle\Entity;

use Symfony\Component\Validator\Constraints as Validate;
use Doctrine\ORM\Mapping as ORM;
use Ulysse\UserBundle\Entity\Utilisateur;
use Doctrine\Common\Collection\ArrayCollection;

/**
 * Commande
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ulysse\FrontBundle\Entity\CommandeRepository")
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;
    
    /**
     * @var Utilisateur
     * 
     * @ORM\ManyToOne(targetEntity="Ulysse\UserBundle\Entity\Utilisateur", cascade={"persist"}, fetch="EAGER")
     * @Validate\NotBlank()
     */
     private $acheteur;

    public function __construct($user = null, $prix = null)
    {
        $this->setAcheteur(($user) ? $user : new Utilisateur());
        $this->setPrix(($prix) ? $prix : 0.0);
        $this->setDate(new \DateTime());
    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Commande
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }
    
    /**
     * Set acheteur
     * 
     * @param  \Ulysse\FrontBundle\Entity\Utilisateur $acheteur
     * @return \Ulysse\FrontBundle\Entity\Commande
     */
    public function setAcheteur(Utilisateur $acheteur)
    {
        $this->acheteur = $acheteur;

        return $this;
    }

        /**
     * Get acheteur
     * 
     * @return \Ulysse\FrontBundle\Entity\Utilisateur
     */
    public function getAcheteur()
    {
        return $this->acheteur;
    }
}
