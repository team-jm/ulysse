function resizeImage(id,h,w)
{
    if (document.getElementById(id))
    {   
        var w2 = document.getElementById(id).naturalWidth;
        var h2 = document.getElementById(id).naturalHeight;
        var proportionUtilisee = 1;
        var proportionH = h/h2;
        var proportionW = w/w2;
        
        /* si l'image actuelle a une dimension supérieure à la case */
        if (proportionH !== 1 || proportionH !== 1)
        {
            /* cas possibles:
            * on a un cadre avec h et w
            * on a une image avec h2 et w2
            * h/h2 => sera <1 si l'image est de base trop haute pour le cadre
            *      => sera >1 si l'image est plus basse
            * w/w2 => sera <1 si l'image est de base trop large pour le cadre
            *      => sera >1 si l'image est plus basse
            */
            rapportProportions = proportionH/proportionW;
            if (proportionH <1 || proportionW <1)
            {
                /* l'image est soit trop haute, soit trop large */

                /* si >1, alors proportionW < proportionH donc c'est la largeur qui a le plus de différence avec le cadre */
                /* si <1, alors proportionH < proportionW donc c'est la hauteur qui a le plus de différence avec le cadre */
                /* on utilise cette différence pour ajuster la taille de l'image par rapport au cadre*/
                
                if (rapportProportions >1)
                {
                    proportionUtilisee = proportionW;
                } else {
                    proportionUtilisee = proportionH;
                }
            } else if (proportionH >1 || proportionW >1)
            {
                /* l'image est soit trop basse, soit trop étroite */

                /* si >1, alors proportionW < proportionH donc c'est la hauteur qui a le plus de différence avec le cadre */
                /* si <1, alors proportionH < proportionW donc c'est la largeur qui a le plus de différence avec le cadre */
                /* on utilise l'inverse de cette différence pour ajuster la taille de l'image par rapport au cadre*/
                
                if (rapportProportions >1)
                {
                    proportionUtilisee = 1/proportionH;
                } else {
                    proportionUtilisee = 1/proportionW;
                }
            }
        }
        document.getElementById(id).style.width = String(w*proportionUtilisee)+'px';
        document.getElementById(id).style.height = String(h*proportionUtilisee)+'px';
    }
}